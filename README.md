## Android Application - barn4221 positioning ##

This README document the steps are necessary to get the application up and running.

### Features ###
* Positioning user's location in ```HKUST barn 4221``` by [Sensoro Beacons](http://www.sensoro.com/)
* Interactive indoor map

### Configuration
  * minSdkVersion 18 (Android 4.3)

### How do I get set up? ###

```ssh
git clone https://kptsui@bitbucket.org/kptsui/barn4221-positioning.git
```
* Or in Android Studio, select "Check out project from Version Control", enter https://kptsui@bitbucket.org/kptsui/barn4221-positioning.git
* All web resources (html, js, css) are placed in ```barn4221-positioning / app / src / main / assets /```

### Library Used:

 * [Leaflet](http://leafletjs.com/)

### Floor Plan

 * Each square side (red line) equals to 1.5m
 * ![alt tag](https://bytebucket.org/kptsui/barn4221-positioning/raw/b8f87f9eadce8aefab8660cc4d4d08d3f14a6309/app/src/main/assets/img/4221FloorPlan.png)

