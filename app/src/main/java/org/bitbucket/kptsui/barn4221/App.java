package org.bitbucket.kptsui.barn4221;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.sensoro.beacon.kit.Beacon;
import com.sensoro.beacon.kit.BeaconManagerListener;
import com.sensoro.cloud.SensoroManager;

import java.util.ArrayList;

/**
 * Created by user on 28/9/2016.
 */

public class App extends Application {
    public final static String TAG = "Application";
    public final static int REQUEST_ENABLE_BLUETOOTH = 111;

    private static App instance;
    // sensoroManager will not be null since it will be created first
    public SensoroManager sensoroManager;

    public final static String SN1 = "0117C5314649"; // my beacon
    public final static String SN2 = "0117C533BAC6"; // my beacon
    public final static String SN3 = "0117C596DBEE"; // my beacon

    /*
    Application will be created first upon all activities or services
     */
    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate()");
        super.onCreate();
        instance = this;

        sensoroManager = SensoroManager.getInstance(this);
        //Enable cloud service (upload sensor data, including battery status, UMM, etc.)。Without setup, it keeps in closed status as default.
        sensoroManager.setCloudServiceEnable(false);

        /*sensoroManager.setBeaconManagerListener(new BeaconManagerListener() {
            @Override
            public void onUpdateBeacon(ArrayList<Beacon> beacons) {
                // 传感器信息更新
                Log.i(TAG, "Update Beacon");

            }

            @Override
            public void onNewBeacon(Beacon beacon) {
                Log.i(TAG, "New Beacon Found - ID: " + beacon.getEddystoneUID() +
                        "\nUrl: " + beacon.getEddystoneURL());

            }

            @Override
            public void onGoneBeacon(Beacon beacon) {
                Log.i(TAG, "Beacon Gone - ID: " + beacon.getEddystoneUID() +
                        "\nUrl: " + beacon.getEddystoneURL());

                Toast.makeText(App.instance, "Beacon Gone: " + beacon.getSerialNumber(), Toast.LENGTH_LONG).show();
            }
        });*/
    }

    @Override
    public void onTerminate() {
        Log.i(TAG, "onTerminate()");
        super.onTerminate();
    }

    public void setBeaconListener(BeaconManagerListener beaconManagerListener){
        Log.i(TAG, "set Beacon Manager Listener");

        sensoroManager.stopService();
        sensoroManager.setBeaconManagerListener(beaconManagerListener);
        startService();
    }

    public void startService(){
        if(sensoroManager.isBluetoothEnabled()){
            try {
                sensoroManager.startService();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(App.instance, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    public static App getInstance(){
        return instance;
    }
}
