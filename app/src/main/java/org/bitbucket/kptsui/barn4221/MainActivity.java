package org.bitbucket.kptsui.barn4221;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.sensoro.beacon.kit.Beacon;
import com.sensoro.beacon.kit.BeaconManagerListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public final static String TAG = "MainActivity";

    WebView mWebView;
    App app;

    Handler mHandler;

    final static double x1 = 312;
    final static double y1 = 133.7;

    final static double x2 = 0;
    final static double y2 = 133.7;

    final static double x3 = 104;
    final static double y3 = 401;

    double[] x = { 0, 0, 0 };
    double[] y = { 0, 0, 0 };
    double[] r = { 0, 0, 0 };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        app = App.getInstance();
        mHandler = new Handler();

        mWebView = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        //mWebView.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "utf-8", null);
        mWebView.loadUrl("file:///android_asset/index.html");

        setBeaconListener();
    }

    @Override
    protected void onStop() {
        super.onStop();
        app.sensoroManager.stopService();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        app.startService();
    }

    public void setBeaconListener(){
        app.setBeaconListener(new BeaconManagerListener() {
            @Override
            public void onUpdateBeacon(ArrayList<Beacon> beacons) {
                // 传感器信息更新
                Log.i(TAG, "Update Beacon");

                // TODO: update webview marker
                try {
                    Beacon[] arr = beacons.toArray(new Beacon[beacons.size()]);
                    sortBeacons(arr);
                    if(arr.length <= 2){
                        return ;
                    }
                    for(int i = 0; i < 3; i++){
                        r[i] = arr[i].getAccuracy();
                        if(arr[i].getSerialNumber().equals(App.SN1)){
                            x[i] = x1;
                            y[i] = y1;
                        }
                        else if(arr[i].getSerialNumber().equals(App.SN2)){
                            x[i] = x2;
                            y[i] = y2;
                        }
                        else if(arr[i].getSerialNumber().equals(App.SN3)){
                            x[i] = x3;
                            y[i] = y3;
                        }
                        else{
                            x[i] = 0;
                            y[i] = 0;
                        }
                    }

                    final String invoke =

                    "javascript:userPosition("+
                            x[0]+","+y[0]+","+r[0]+","+
                            x[1]+","+y[1]+","+r[1]+","+
                            x[2]+","+y[2]+","+r[2]+")";

                    Log.d(TAG, invoke);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mWebView.loadUrl(invoke);
                        }
                    });
                } catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(app, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNewBeacon(Beacon beacon) {
                Log.i(TAG, "New Beacon Found - ID: " + beacon.getEddystoneUID() +
                        "\nUrl: " + beacon.getEddystoneURL());

            }

            @Override
            public void onGoneBeacon(Beacon beacon) {
                Log.i(TAG, "Beacon Gone - ID: " + beacon.getEddystoneUID() +
                        "\nUrl: " + beacon.getEddystoneURL());

                Toast.makeText(app, "Beacon Gone: " + beacon.getSerialNumber(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void sortBeacons(Beacon[] beacons){
        int n = beacons.length;
        Beacon temp = null;

        for(int i=0; i < n; i++){
            for(int j=1; j < (n-i); j++){
                if(beacons[j-1].getAccuracy() > beacons[j].getAccuracy()){
                    //swap the elements!
                    temp = beacons[j-1];
                    beacons[j-1] = beacons[j];
                    beacons[j] = temp;
                }

            }
        }
    }
}
