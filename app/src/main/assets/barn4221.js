var map = null;
var marker = null;

$(document).ready(function(){
	
	map = L.map('mapid', {
		crs: L.CRS.Simple
	});

	var bounds = [[0,0], [312,401]];
	var image = L.imageOverlay('img/4221FloorPlan.png', bounds).addTo(map);

	map.fitBounds(bounds);
	
	var driver = L.latLng([ 145, 175.2 ]);
	marker = L.marker(driver);
	marker.addTo(map).bindPopup('driver');
	map.setView( [145, 175.2], 1);
});

var height = 1.05;

function userPosition(ax=0,ay=0,ar=10,bx=10,by=20,br=10,cx=20,cy=10,cr=10){
	ar = filterRadius(ar);
	br = filterRadius(br);
	cr = filterRadius(cr);

	var circ_A = new Circ("a",ax,ay,ar);
	var circ_B = new Circ("b",bx,by,br);
	var circ_C = new Circ("a",cx,cy,cr);

	var Bisec_AB = calBisectPoint(circ_A, circ_B);
	var Bisec_BC = calBisectPoint(circ_B, circ_C);
	var Bisec_AC = calBisectPoint(circ_A, circ_C);

	var cal_V = calOrthocentre(Bisec_AB, Bisec_BC, Bisec_AC);

	// setting position
	map.removeLayer(marker)

	var driver = L.latLng([ cal_V.x, cal_V.y ]);
	marker = L.marker(driver);
    	marker.addTo(map).bindPopup('driver');
	map.setView( [cal_V.x, cal_V.y], 1);

	return cal_V;
}


//Point
function Point(name="", x=0, y=0){
	this.name = name;
	this.x = x;
	this.y = y;
}

Point.prototype.getInfo = function() {
    return this.name+" ( "+this.x+", "+this.y+" )";
};

//Circle
function Circ(name="", x=0, y=0, r=0){
	this.name = name;
	this.x = x;
	this.y = y;
	this.r = r;
	this.center = new Point(name, x, y);
}

Circ.prototype.getCenter = function(){
		return this.point;
};

Circ.prototype.getInfo = function() {
    return this.name+" ( "+this.x+", "+this.y+", "+this.r+", ( "+this.center.x+","+this.center.y+" ) )";
};

//Math
function sq(a){
	return Math.pow(a,2);
}

function sqrt(a){
	return Math.sqrt(a);
}

function filterRadius(a){
	if(a <= height){
		return a;
	} else{
		return sqrt(sq(a)-sq(height));
	}
}

function solveQuadEqua(a,b,c,ans){
	var ans_1=0;
	var ans_2=0;

	var delta = sq(b) - 4 * a * c;

	if (delta > 0){
		ans_1 = (-b+sqrt(delta))/(2*a);
		ans_2 = (-b-sqrt(delta))/(2*a);
	} else if(delta ==0){
		ans_1 = ans_2 = (-b+sqrt(delta))/(2*a);
	} else{
		ans_1 = ans_2 = "no solutions"
	}

	if(ans == 1){
			return ans_1;
		} else {
			return ans_2;
		}
}

function calBisectPoint(circA, circB){
	var circleBisectPoint = new Point("Bisect"+circA.name+circB.name);

	var ax = circA.x;
	var ay = circA.y;
	var ar = circA.r;
	var bx = circB.x;
	var by = circB.y;
	var br = circB.r;

	var a = (sq(ay)-2*ay*by+sq(by))/sq(ax-bx) + 1;
	var b = (-2*ax*sq(ay)+4*ax*ay*by-2*ax*sq(by))/sq(ax-bx) - 2*ax;
	var c = (sq(ax*ay)-2*sq(ax)*ay*by+sq(ax*by))/sq(ax-bx) - sq(ar/(ar+br))*(sq(ay-by)+sq(ax-bx)) + sq(ax);
	var m_1 = solveQuadEqua(a,b,c,1);
	var m_2 = solveQuadEqua(a,b,c,2);

	if(ax>bx){ var l_circ_x =bx, r_circ_x = ax; }else{var l_circ_x =ax, r_circ_x = bx; }
	if(l_circ_x<m_1 && m_1 < r_circ_x){circleBisectPoint.x = m_1; }else{circleBisectPoint.x = m_2;}

	circleBisectPoint.y = (ay-by)/(ax-bx)*(circleBisectPoint.x-ax)+ay;

	return circleBisectPoint;
}

function calOrthocentre(Bisect1, Bisect2, Bisect3){
	var orthocentre = new Point("Orthocentre");
	var a, b;
	a = (Bisect2.x-Bisect1.x)/(Bisect1.y-Bisect2.y);
	b = (Bisect3.x-Bisect1.x)/(Bisect1.y-Bisect3.y);
	orthocentre.x = (Bisect3.x*a-Bisect2.x*b-Bisect3.y+Bisect2.y)/(a-b);
	orthocentre.y = (orthocentre.x-Bisect3.x)*a+Bisect3.y;
	return orthocentre;
}

//Calibration
function changeHeight(a){
 height = a;
}